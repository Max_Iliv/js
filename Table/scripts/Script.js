var names = ["Ben", "Bernard", "Alex"];
var pos = ["Dev", "SEO", "Manager"];

function add(){
	var name = document.getElementById("IName").value;
	var position = document.getElementById("IPos").value;

	names.push(name);
	pos.push(position);

	document.getElementById("IName").value ="";
	document.getElementById("IPos").value = "";
}

function set() {
	for(var i=0; i<names.length; i++){
		document.getElementById("data").innerHTML += "<tr><td>"+(i+1)+"</td><td>"+names[i]+"</td><td>"+pos[i]+"</td></tr>";
	}
}

function refresh(){
	document.getElementById("data").innerHTML = "";
	set();
}

function setInt(){
	setInterval(Time, 1000);
}

function Time(){
	var d = new Date();
	document.getElementById("time").innerHTML = d.getHours() +":"+ d.getMinutes()+":"+ d.getSeconds(); 
}
